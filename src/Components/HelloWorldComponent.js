import React from 'react'
import classes from './helloWorldComponent.module.css'

const HelloWorldComponent = ({ text, data }) => {
  return (
        <div>
            <div className={classes.block}>
                {text}
            </div>
        </div>
  )
}

export default HelloWorldComponent
