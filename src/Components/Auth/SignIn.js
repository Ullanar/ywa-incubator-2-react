import React from 'react'

const SignIn = ({ setCurrentAuthComponent }) => {
  return (
        <div style={{ display: 'flex', flexDirection: 'column', width: '30%' }}>
            <h2>Sign In</h2>
            <input placeholder='Email'/>
            <input placeholder='Password' type='password'/>
            <button>Sign In</button>
            <button onClick={() => setCurrentAuthComponent('SIGN_UP')}>Still not registered? Sign up now!</button>
        </div>
  )
}

export default SignIn
