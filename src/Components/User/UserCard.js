import React from 'react'
import { observer } from 'mobx-react-lite'
import { UsersState } from '../../Store/UserStore'

const UserCard = observer(({ user }) => {
  return (
        <div style={{ marginTop: '20px' }}>
            <div>{user.name}</div>
            <div>{user.email}</div>
            {UsersState.users.length}
        </div>
  )
})

export default UserCard
