import { user } from './users'
import { games } from './games'
import axios from 'axios'

export const API_GATEWAY = 'https://jsonplaceholder.typicode.com'

export const apiRoot = {
  user,
  games,
  getUsers: () => axios.get(`${API_GATEWAY}/users`)
}
