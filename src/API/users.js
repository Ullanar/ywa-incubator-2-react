import axios from 'axios'
import { API_GATEWAY } from './index'

export const user = {
  getUsers: () => axios.get(`${API_GATEWAY}/users`)
}
