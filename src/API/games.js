import axios from 'axios'

export const games = {
  getAllGames: () => axios.get('http://localhost:8080/get-all-gamees'),
  getGameById: (gameId) => axios.get(`http://localhost:8080/get-game-by-id/${gameId}`)
}
