import './app.css'
import React, {useEffect, useState} from 'react'
import { apiRoot } from './API'
import UserCard from './Components/User/UserCard'
import { observer } from 'mobx-react-lite'
import { UsersState } from './Store/UserStore'
import SignUp from './Components/Auth/SignUp'
import SignIn from './Components/Auth/SignIn'

const App = observer(() => {
  const [currentAuthComponent, setCurrentAuthComponent] = useState('SIGN_UP')

  const getAllUsers = async () => {
    const res = await apiRoot.user.getUsers()
    UsersState.setUsers(res.data)
  }

  useEffect(() => {
    getAllUsers()
  }, [])

  return (
      <div className="App">

        {/* {UsersState.users.map(user => <UserCard user={user} key={user.id}/>)} */}

         {/* <HelloWorldComponent text={'Hello World'} data={[1, 2, 3]}/> */}

         {
            currentAuthComponent === 'SIGN_UP'
              ? <SignUp setCurrentAuthComponent={setCurrentAuthComponent}/>
              : <SignIn setCurrentAuthComponent={setCurrentAuthComponent}/>
         }
      </div>
  )
})

export default App
